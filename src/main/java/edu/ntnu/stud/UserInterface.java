package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;

/** UserInterface-class that handles the interaction with the user, and prompts the register class
 * to execute methods.
 *
 * @author Therese Synnøve Rondeel
 */
public class UserInterface {
  private TrainDepartureRegister trainDepartureRegister;
  private Scanner input;

  // Constants for the menu.
  private final int PRINT_REGISTER = 1;
  private final int ADD_DEPARTURE = 2;
  private final int REMOVE_DEPARTURE = 3;
  private final int SET_CURRENT_TIME = 4;
  private final int SET_TRACK = 5;
  private final int SET_DELAY = 6;
  private final int GET_DEPARTURE_BY_TRAINNO = 7;
  private final int GET_DEPARTURES_BY_DESTINATION = 8;
  private final int GET_DEPARTURES_BY_TRACK = 9;
  private final int PRINT_INFORMATION_BOARD = 10;
  private final int EXIT = 11;

  /** Method for initializing both the scanner and the register. Also calls the fillRegister-method.
   *
   * @see #fillRegister() fillRegister
   */
  public void init() {
    input = new Scanner(System.in);
    trainDepartureRegister = new TrainDepartureRegister();
    fillRegister();
  }

  /** Method for filling the register with dummy train departures. Also sets the delay for some.
   */
  private void fillRegister() {
    trainDepartureRegister.addDeparture(123, LocalTime.of(15, 0), 2,
        "F6", "Trondheim");
    trainDepartureRegister.addDeparture(456, LocalTime.of(14, 45), 1,
        "R11", "Skien");
    trainDepartureRegister.addDeparture(789, LocalTime.of(18, 15), 3,
        "L2", "Ski");
    trainDepartureRegister.addDeparture(101, LocalTime.of(8, 13), 2,
        "F4", "Bergen");

    trainDepartureRegister.setDelay(456, LocalTime.of(0, 15));
    trainDepartureRegister.setDelay(101, LocalTime.of(1, 20));
  }

  /** Method for starting the application. As long as the user doesn't enter the value of EXIT, the
   * application will not stop. A boolean is used to indicate if the application should stop. This
   * method also calls on the showMenu-method.
   *
   * @see #showMenu() showMenu
   */
  public void start() {
    boolean finished = false;

    do {
      int menuChoice = this.showMenu();

      switch (menuChoice) {
        case PRINT_REGISTER:
          printRegister();
          break;
        case ADD_DEPARTURE:
          userAddDeparture();
          break;
        case REMOVE_DEPARTURE:
          userRemoveDeparture();
          break;
        case SET_CURRENT_TIME:
          userSetCurrentTime();
          break;
        case SET_TRACK:
          userSetTrack();
          break;
        case SET_DELAY:
          userSetDelay();
          break;
        case GET_DEPARTURE_BY_TRAINNO:
          userGetDepartureByTrainNo();
          break;
        case GET_DEPARTURES_BY_DESTINATION:
          userGetDeparturesByDestination();
          break;
        case GET_DEPARTURES_BY_TRACK:
          userGetDeparturesByTrack();
          break;
        case PRINT_INFORMATION_BOARD:
          printInformationBoard();
          break;
        case EXIT:
          System.out.println("You have ended this session of using the Train Dispatch App.");
          finished = true;
          break;
        default:
          System.out.println("Something went wrong. Make sure to enter a number between 1 and 11.");
          break;
      }
    } while (!finished);
  }

  /** Method for showing the menu to the user, and getting their choice of task.
   *
   * @return the input from the user, by using the getUserIntInput-method
   * @see #getUserIntInput() getUserIntInput
   */
  private int showMenu() {
    System.out.println("\n***** TRAIN DEPARTURE REGISTER ***** CURRENT TIME: "
        + trainDepartureRegister.getCurrentTime() + " *****");

    System.out.println("***** MENU *****");
    System.out.println("1. Show all existing train departures");
    System.out.println("2. Add departure");
    System.out.println("3. Remove departure");
    System.out.println("4. Change the current time");
    System.out.println("5. Set/Change the track of a departure");
    System.out.println("6. Set/Change the delay of a departure");
    System.out.println("7. Get information of a departure by the train number");
    System.out.println("8. Get the departures to a specific destination");
    System.out.println("9. Get departures by track");
    System.out.println("10. Show information board");
    System.out.println("11. Exit the application.");

    return getUserIntInput();
  }

  /** Method for getting an int-value from the user. It will run as long as the user hasn't entered
   * a number.
   *
   * @return int-value from user
   */
  private int getUserIntInput() {
    int intInput = 0;
    boolean isValidValue = false;
    do {
      try {
        intInput = Integer.parseInt(input.nextLine());
        isValidValue = true;
      } catch (Exception e) {
        System.out.println("Invalid input: Make sure to write a number. \nPlease enter again: ");
      }
    } while (!isValidValue);
    return intInput;
  }

  /** Method for getting a LocalTime-input from the user. It uses the getUserIntInput-method. If
   * the LocalTime.of(hour, minute) is not able to turn the numbers into a time, the user will be
   * prompted to enter again until a valid time has been entered.
   *
   * @return LocalTime-value from user
   */
  private LocalTime getUserLocalTimeInput() {
    LocalTime timeInput = LocalTime.of(0, 0);
    boolean isValidValue = false;
    do {
      System.out.println("First, please enter the amount of hours: ");
      int hour = getUserIntInput();

      System.out.println("Now please enter the amount of minutes: ");
      int minute = getUserIntInput();
      try {
        timeInput = LocalTime.of(hour, minute);
        isValidValue = true;
      } catch (Exception e) {
        System.out.println("Invalid input: Make sure that the requested hour is between 0 and 23, "
            + "and the minutes between 0 and 59. \nPlease enter again. ");
      }
    } while (!isValidValue);
    return timeInput;
  }

  /** Method for getting a String-input.
   *
   * @return String-input from the user
   */
  private String getUserStringInput() {
    return input.nextLine();
  }

  /** Method for printing the whole register to the user. Before printing, it controls whether there
   * are any departures registered.
   */
  private void printRegister() {
    if (trainDepartureRegister.toString().isEmpty()) {
      System.out.println("Could not find any departures in the register.");
    } else {
      System.out.println("\n***** ALL REGISTERED DEPARTURES ***** "
          + trainDepartureRegister.getCurrentTime() + " *****");
      System.out.println(getHeader());
      System.out.println(trainDepartureRegister.toString());
    }
  }

  /** Method for letting the user add a departure. It first gathers all the relevant information,
   * and then tries to add the departure. It catches exceptions in case of invalid input.
   */
  private void userAddDeparture() {
    System.out.println("\n***** ADD DEPARTURE *****");

    System.out.println("Please enter the train number: ");
    int trainNo = getUserIntInput();

    System.out.println("Please do the following to enter the departure time: ");
    LocalTime departureTime = getUserLocalTimeInput();

    System.out.println("Please enter the number of the track (enter 0 if there isn't an assigned "
        + "track yet): ");
    int track = getUserIntInput();

    System.out.println("Please enter the line: ");
    String line = getUserStringInput();

    System.out.println("Please enter the destination: ");
    String destination = getUserStringInput();

    try {
      if (track == 0) {
        trainDepartureRegister.addDeparture(trainNo, departureTime, line, destination);
      } else {
        trainDepartureRegister.addDeparture(trainNo, departureTime, track, line, destination);
      }
      System.out.println("The departure was successfully added!");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /** Method for the user to remove a specific departure. It will try to remove this departure, and
   * will catch an exception in case of invalid input.
   */
  private void userRemoveDeparture() {
    System.out.println("\n***** REMOVE DEPARTURE *****");

    System.out.println("Please enter the train number of the train you wish to remove: ");
    int trainNo = getUserIntInput();

    try {
      trainDepartureRegister.removeDepartureByTrainNo(trainNo);
      System.out.println("The train with train number '" + trainNo + "' was successfully removed.");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }

  /** Method for the user to set the current time. Exceptions thrown caused by invalid input will
   * be caught.
   */
  private void userSetCurrentTime() {
    System.out.println("\n***** SET CURRENT TIME *****");

    System.out.println("Please do the following to enter the current time: ");
    LocalTime newTime = getUserLocalTimeInput();

    try {
      trainDepartureRegister.setCurrentTime(newTime);
      System.out.println("The current time was successfully changed!");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /** Method for the user to set the track of a departure. Exceptions thrown caused be invalid
   * input will be caught.
   */
  private void userSetTrack() {
    System.out.println("\n***** SET TRACK *****");

    System.out.println("Please enter the train number you wish to set or change the track for: ");
    int trainNo = getUserIntInput();

    System.out.println("Please enter the track: ");
    int track = getUserIntInput();

    try {
      trainDepartureRegister.setTrack(trainNo, track);
      System.out.println("The track was successfully set/changed!");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /** Method for the user to set the delay of a departure. Exceptions thrown caused by invalid
   * input will be caught.
   *
   */
  private void userSetDelay() {
    System.out.println("\n***** SET DELAY *****");

    System.out.println("Please enter the train number you wish to set or change the delay for: ");
    int trainNo = getUserIntInput();

    System.out.println("Please do the following to enter the delay: ");
    LocalTime delay = getUserLocalTimeInput();

    try {
      trainDepartureRegister.setDelay(trainNo, delay);
      System.out.println("The delay was successfully set/changed!");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /** Method for the user to get the information on a specific train departure. Will inform the
   * user if there wasn't found a train departure with the requested train number.
   */
  private void userGetDepartureByTrainNo() {
    System.out.println("\n***** INFORMATION ON SPECIFIC TRAIN DEPARTURE *****");

    System.out.println("Please enter the desired train number: ");
    int trainNo = getUserIntInput();

    if (trainDepartureRegister.getDepartureByTrainNo(trainNo) == null) {
      System.out.println("Could not find a departure with the requested train number: " + trainNo);
    } else {
      System.out.println("REQUESTED TRAIN DEPARTURE:");
      System.out.println(getHeader());
      System.out.println(trainDepartureRegister.getDepartureByTrainNo(trainNo).toString());
    }
  }

  /** Method for the user to get all departures to a specific destination. Exceptions caused by
   * invalid input will be caught.
   */
  private void userGetDeparturesByDestination() {
    System.out.println("\n***** DEPARTURES BY DESINATION *****");

    System.out.println("Please enter the desired destination: ");
    String destination = getUserStringInput();

    if (trainDepartureRegister.getDeparturesByDestination(destination) == null) {
      System.out.println("Could not find departures with the requested destination.");
    } else {
      System.out.println("***** DEPARTURES TO " + destination.toUpperCase() + " *****");
      List<TrainDeparture> departuresByDestination =
          trainDepartureRegister.getDeparturesByDestination(destination);
      System.out.println(printList(departuresByDestination));
    }
  }

  /** Method for the user to get all departures from a specific track. Will also have the ability
   * to see all departures without an assigned track. Exceptions caught by invalid input will be
   * caught.
   */
  private void userGetDeparturesByTrack() {
    System.out.println("***** GET DEPARTURES BY TRACK *****");

    System.out.println("Please enter the desired track (enter -1 if you want to see all departures"
        + " with no assigned track): ");
    int track = getUserIntInput();

    if (trainDepartureRegister.getDeparturesByTrack(track) == null) {
      System.out.println("Could not find departures with the requested track.");
    } else {
      if (track == -1) {
        System.out.println("***** DEPARTURES WITH NO ASSIGNED TRACK *****");
      } else {
        System.out.println("***** DEPARTURES FROM TRACK " + track + " *****");
      }
      List<TrainDeparture> departuresByTrack = trainDepartureRegister.getDeparturesByTrack(track);
      System.out.println(printList(departuresByTrack));
    }
  }

  /** Method for printing the information board to the user. All departures with assigned tracks
   * will be printed. Will inform the user if there aren't any departures with assigned tracks.
   */
  private void printInformationBoard() {
    if (trainDepartureRegister.getDeparturesWithTrack() == null) {
      System.out.println("Could not find any departures with assigned tracks.");
    } else {
      System.out.println("\n***** INFORMATION BOARD ***** "
          + trainDepartureRegister.getCurrentTime() + " *****");
      List<TrainDeparture> departuresWithTrack = trainDepartureRegister.getDeparturesWithTrack();
      System.out.println(printList(departuresWithTrack));
    }
  }

  /** Method for turning a List into a String. Uses a StringBuilder and forEach to achieve
   * this.
   *
   * @param listRegister a list with train departures
   * @return a printable String with all the departures in the List-parameter
   */
  private String printList(List<TrainDeparture> listRegister) {
    StringBuilder string = new StringBuilder();
    string.append(getHeader()).append("\n");
    listRegister.forEach(trainDeparture -> string.append(trainDeparture.toString()).append("\n"));
    return string.toString();
  }

  /** Method for getting a header. This method is utilized when printing one or several departures
   * to the user.
   *
   * @return a header in the form of a String
   */
  private String getHeader() {
    return String.format("%-13s%-10s%-10s%-10s%-17s%-8s",
        "Departure", "Delay", "Line", "Train No", "Destination", "Track");
  }
}