package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/** Register class that represents a register of train departures.
 * This class contains one constructor that initializes both register and the current time of the
 * application. The class also contains getters for the fields, as well as a set-method for the
 * time. In addition, the class contains several methods for adding, removing, sorting and getting
 * specific assortments of the register. Lastly, this class uses the TrainDeparture-class for
 * adding new departures to the register.
 *
 * @see TrainDeparture
 *
 * @author Therese Synnøve Rondeel
 */
public class TrainDepartureRegister {
  /**
   * The register that is to contain all train departures. The key will be the train number, while
   * the value will be an object of the TrainDeparture-class.
   *
   * @see TrainDeparture
   */
  private final Map<Integer, TrainDeparture> trainDepartureRegister;

  /**
   * The current time that is to be set and changed by the user.
   */
  private LocalTime currentTime;

  /** Constructor for TrainDepartureRegister-class.
   * This constructor initializes a register of train departures, as well as a LocalTime-variable.
   * The time is set to 00:00, and will need to be changed while using the application.
   */
  public TrainDepartureRegister() {
    this.trainDepartureRegister = new HashMap<>();
    this.currentTime = LocalTime.MIN;
  }

  /** Get-method for accessing the train departure register.
   *
   * @return the train departure register
   */
  public Map<Integer, TrainDeparture> getTrainDepartureRegister() {
    return trainDepartureRegister;
  }

  /** Get-method for the current time.
   *
   * @return the current time
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /** Method for adding a departure to the register, with a track parameter. Will throw if any of
   * the values of the parameters are found to be invalid. If all parameters are valid, a new
   * departure will be created and added to the register by using HashMap's own "put"-method. The
   * train number is used for the key, while the value is an object of the TrainDeparture-class.
   *
   * @param trainNo the requested train number for the departure
   * @param departureTime the requested departure time
   * @param track the requested track for the departure
   * @param line the name of the stretch the train is travelling
   * @param destination the end-destination for where the train is traveling to
   * @throws IllegalArgumentException if there already exists a departure with the requested train
   *                                  number, or if the requested departure time is before the
   *                                  current one. Will also throw from TrainDeparture-class if the
   *                                  values of the parameters are not valid.
   * @see TrainDeparture#TrainDeparture(int, LocalTime, int, String, String)
   */
  public void addDeparture(int trainNo, LocalTime departureTime, int track, String line,
                           String destination) throws IllegalArgumentException {
    if (checkTrainDepartureExists(trainNo)) {
      throw new IllegalArgumentException("Invalid input: There already exists a train departure "
          + " with the requested train number");
    }
    if (departureTime.isBefore(currentTime)) {
      throw new IllegalArgumentException("Invalid input: The requested departure time is before the"
          + " current time.");
    }

    TrainDeparture trainDeparture = new TrainDeparture(trainNo, departureTime, track, line,
        destination);
    trainDepartureRegister.put(trainNo, trainDeparture);
  }

  /** Method for adding a departure to the register, without a track parameter. Will throw if any of
   * the values of the parameters are found to be invalid. If all parameters are valid, a new
   * departure will be created and added to the register by using HashMap's own "put"-method. The
   * train number is used for the key, while the value is an object of the TrainDeparture-class.
   *
   * @param trainNo the requested train number for the departure
   * @param departureTime the requested departure time
   * @param line the name of the stretch the train is traveling
   * @param destination the end-destination for where the train is traveling to
   * @throws IllegalArgumentException if there already exists a departure with the requested train
   *                                  number, or if the requested departure time is before the
   *                                  current one. Will also throw from TrainDeparture-class if the
   *                                  values of the parameters are not valid.
   * @see TrainDeparture
   * @see TrainDeparture#TrainDeparture(int, LocalTime, String, String)
   */
  public void addDeparture(int trainNo, LocalTime departureTime, String line,
                           String destination) throws IllegalArgumentException {
    if (checkTrainDepartureExists(trainNo)) {
      throw new IllegalArgumentException("Invalid input: There already exists a train departure "
          + " with the requested train number");
    }
    if (departureTime.isBefore(currentTime)) {
      throw new IllegalArgumentException("Invalid input: The requested departure time is before the"
          + " current time.");
    }

    TrainDeparture trainDeparture = new TrainDeparture(trainNo, departureTime, line,
        destination);
    trainDepartureRegister.put(trainNo, trainDeparture);
  }

  /** Method to get a specific departure by the train number. It uses HashMap's own get-method by
   * accessing the value through the key, which is the trains number.
   *
   * @param trainNo the train number of the train departure
   * @return null if there doesn't exist a train departure with the requested train number. If it
   *         exists, the train departure is returned
   */
  public TrainDeparture getDepartureByTrainNo(int trainNo) {
    if (!checkTrainDepartureExists(trainNo)) {
      return null;
    }

    return trainDepartureRegister.get(trainNo);
  }

  /** Mutator-method for setting the current time. It first checks whether the requested new time
   * is before the current one. If it doesn't throw, the current time is set to be equal to the new
   * time. After this, the removeDepartures-method is executed.
   *
   * @param newTime the new current-time requested by the user
   * @throws IllegalArgumentException throws exception if the requested time is before the current
   * @see #removeDepartures()
   */
  public void setCurrentTime(LocalTime newTime) throws IllegalArgumentException {
    if (newTime.isBefore(currentTime)) {
      throw new IllegalArgumentException("Invalid input: The requested time is before the current "
          + "one.");
    }
    currentTime = newTime;
    removeDepartures();
  }

  /** Mutator-method for setting the delay of a specific train departure. It first checks if there
   * exists a train departure with the requested train number. If not it tries to set the delay,
   * but will throw from the TrainDeparture class' setDelay-method in case of invalid delay.
   *
   * @param trainNo the train number of the train departure
   * @param delay the requested delay to set to the train departure
   * @throws IllegalArgumentException if there doesn't exist a train departure with the requested
   *                                  train number. Will also throw from TrainDeparture's setDelay-
   *                                  method if the requested delay would make the train leave past
   *                                  midnight.
   * @see TrainDeparture#setDelay(LocalTime)
   */
  public void setDelay(int trainNo, LocalTime delay) throws IllegalArgumentException {
    if (!checkTrainDepartureExists(trainNo)) {
      throw new IllegalArgumentException("Invalid input: Could not find a departure with the "
          + "requested train number.");
    }

    TrainDeparture trainDeparture = getDepartureByTrainNo(trainNo);
    trainDeparture.setDelay(delay);
  }

  /** Mutator-method for setting the track of a specific train departure. It first checks if there
   * exists a train departure with the requested train number. If not it tries to set the track, but
   * will throw from the TrainDeparture class' setTrack-method in case of invalid track.
   *
   * @param trainNo the train number of the train departure
   * @param track the requested track to set for the train departure
   * @throws IllegalArgumentException if there doesn't exist a train departure with the requested
   *                                  train number. Will also throw from TrainDeparture's setTrack-
   *                                  method if the track is invalid.
   * @see TrainDeparture#setTrack(int)
   */
  public void setTrack(int trainNo, int track) throws IllegalArgumentException {
    if (!checkTrainDepartureExists(trainNo)) {
      throw new IllegalArgumentException("Invalid input: Could not find a departure with the "
          + "requested train number.");
    }

    TrainDeparture trainDeparture = getDepartureByTrainNo(trainNo);
    trainDeparture.setTrack(track);
  }

  /** Method for removing all departures that have a departure time, inclusive the delay, that is
   * before the current time. The method uses an Iterator to lessen the chance of the
   * ConcurrentModificationException being thrown. This method was made by ChatGPT.
   */
  private void removeDepartures() {
    Iterator<Map.Entry<Integer, TrainDeparture>> iterator =
        trainDepartureRegister.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<Integer, TrainDeparture> entry = iterator.next();
      if (entry.getValue().getDepartureTimeWithDelay().isBefore(currentTime)) {
        iterator.remove();
      }
    }
  }

  /** Method for removing a specific train departure from the HashMap. It throws if there doesn't
   * already exist a train departure with the requested train number. If not, HashMap's own
   * remove-method is used.
   *
   * @param trainNo the train number of the train departure that is requested to remove
   * @throws IllegalArgumentException if there doesn't exist a departure with the requested train
   *                                  number.
   */
  public void removeDepartureByTrainNo(int trainNo) throws IllegalArgumentException {
    if (!checkTrainDepartureExists(trainNo)) {
      throw new IllegalArgumentException("Invalid input: Could not find the requested train "
          + "number.");
    }
    trainDepartureRegister.remove(trainNo);
  }

  /** Method for checking whether there exists a train departure with the key-identifying train
   * number in the HashMap.
   *
   * @param trainNo the train number of the requested train departure
   * @return true or false depending on if the HashMap contains the requested key
   */
  public boolean checkTrainDepartureExists(int trainNo) {
    return trainDepartureRegister.containsKey(trainNo);
  }

  /** Method for getting a list of all train departures to a specific destination. The parameter is
   * first turned to lower case for improved user experience. Streams is then used to filter all
   * departures that have the same destination, also in lower case.
   *
   * @param destination the end-destination for the departures
   * @return sorted list of all departures to the requested destination, returns null if no such
   *         departures could be found
   * @see #sortByTime(List)
   */
  public List<TrainDeparture> getDeparturesByDestination(String destination) {
    String lowerCaDestination = destination.toLowerCase();
    List<TrainDeparture> departuresByDestination =
        new ArrayList<>(trainDepartureRegister.values().stream()
        .filter(trainDeparture -> trainDeparture.getDestination().toLowerCase()
            .equals(lowerCaDestination))
        .toList());
    if (departuresByDestination.isEmpty()) {
      return null;
    }
    sortByTime(departuresByDestination);
    return departuresByDestination;
  }

  /** Method for getting a list of all train departures from a specific track. It is possible to
   * enter -1 to get all departures without an assigned track. Streams is then used to filter all
   * departures leaving from the requested track.
   *
   * @param track the requested track to find all departures from
   * @return sorted list of all departures from the requested track, returns null if no such
   *         departures could be found
   * @see #sortByTime(List) 
   */
  public List<TrainDeparture> getDeparturesByTrack(int track) {
    List<TrainDeparture> departuresByTrack = new ArrayList<>(trainDepartureRegister.values()
        .stream()
        .filter(trainDeparture -> trainDeparture.getTrack() == track)
        .toList());
    if (departuresByTrack.isEmpty()) {
      return null;
    }

    sortByTime(departuresByTrack);
    return departuresByTrack;
  }

  /** Method for getting a list of all departures with an assigned track. Streams is used to filter
   * out all departures that do not equal to -1, indicating no assigned track.
   *
   * @return sorted list of all departures with assigned track, returns null if no such departures
   *         could be found.
   * @see #sortByTime(List) 
   */
  public List<TrainDeparture> getDeparturesWithTrack() {
    List<TrainDeparture> departuresWithTrack =
        new ArrayList<>(trainDepartureRegister.values().stream()
        .filter(trainDeparture -> trainDeparture.getTrack() != -1)
        .toList());

    if (departuresWithTrack.isEmpty()) {
      return null;
    }

    sortByTime(departuresWithTrack);
    return departuresWithTrack;
  }

  /** Method for sorting a list of train departures by their departure time. Before using the
   * .sort-method, a Comparator for the TrainDeparture-class is made. This compares the departure
   * time between the train departures. The register-parameter is then sorted after the conditions
   * in the comparator.
   *
   * @param register list of train departures that are to be sorted
   */
  public void sortByTime(List<TrainDeparture> register) {
    Comparator<TrainDeparture> sortByTime = (TrainDeparture t1, TrainDeparture t2) ->
        t1.getDepartureTime().compareTo(t2.getDepartureTime());
    register.sort(sortByTime);
  }

  @Override
  public String toString() {
    List<TrainDeparture> register = new ArrayList<>(trainDepartureRegister.values());
    if (register.isEmpty()) {
      return "";
    }
    sortByTime(register);

    StringBuilder string = new StringBuilder();
    register.forEach((TrainDeparture departure) -> string.append(departure.toString())
        .append("\n"));
    return string.toString();
  }
}
