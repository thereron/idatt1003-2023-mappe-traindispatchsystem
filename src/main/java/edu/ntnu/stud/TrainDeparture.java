package edu.ntnu.stud;

import java.time.LocalTime;

/** Train departure class that represents a train departure.
 * This class contains two constructors, one that takes a track parameter, and one that sets track
 * to -1, indicating that a departure hasn't been assigned a track yet. The class also contains
 * getters for all attributes, as well as setters for those that are relevant. It also includes a
 * get-method for departure time with delay. Lastly it has a toString-method that may be printed to
 * the user in UserInterface.
 *
 * @see UserInterface
 *
 * @author Therese Synnøve Rondeel
 */
public class TrainDeparture {
  /**
   * The number for a specific train departure.
   */
  private final int trainNo;

  /**
   * The time the train leaves the station. Does not account for the delay.
   */
  private final LocalTime departureTime;

  /**
   * The delay of the train departure.
   */
  private LocalTime delay;

  /**
   * The track the train will leave from.
   */
  private int track;

  /**
   * The name of the stretch the train is traveling.
   */
  private final String line;

  /**
   * The name of the end-destination for where the train is traveling to.
   */
  private final String destination;

  /** Constructor for TrainDeparture class.
   * The constructor initializes a departure with the parameters below. The delay of the departure
   * is set to MIN (which is 00:00). Before assigning the values, the validateValues-method is used
   * to make sure trainNo, hour and minute are valid for this application.
   *
   * @param trainNo the number for this specific train departure
   * @param departureTime the time the train is to leave the station
   * @param track the track the train will leave from
   * @param line the name of the stretch the train is traveling
   * @param destination the end-destination for where the train is traveling to.
   * @throws IllegalArgumentException if any of the validate-methods detect an invalid value
   *
   * @see #validateTrainNo(int)
   * @see #validateTrack(int)
   * @see #validateString(String)
   */
  public TrainDeparture(int trainNo, LocalTime departureTime,
                        int track, String line, String destination)
      throws IllegalArgumentException {
    validateTrainNo(trainNo);
    validateTrack(track);
    validateString(line);
    validateString(destination);

    this.trainNo = trainNo;
    this.departureTime = departureTime;
    this.delay = LocalTime.MIN;
    this.track = track;
    this.line = line;
    this.destination = destination;
  }

  /** Constructor for TrainDeparture class.
   * The constructor initializes a departure by using the constructor above. This constructor
   * allows the creation of a departure without a specified track, which is indicated by assigning
   * -1 to the track. Due to using the constructor above, the delay is set to zero here as well.
   * <p>This method was improved with the help of ChatGPT. The code was originally similar to the
   * one above, but ChatGPT was asked how to lessen the duplication of code when having similar
   * constructors. The information received was implemented, and the usage of this() was therefore
   * used to lessen the amount of duplication of code. </p>
   *
   * @param trainNo the number for this specific train departure
   * @param departureTime the time the train is to leave the station
   * @param line the name of the stretch the train is traveling
   * @param destination the end-destination for where the train is traveling to.
   * @throws IllegalArgumentException if any of the validate-methods detect an invalid value
   * @see #TrainDeparture(int, LocalTime, int, String, String)
   */
  public TrainDeparture(int trainNo, LocalTime departureTime, String line, String destination)
      throws IllegalArgumentException {
    this(trainNo, departureTime, -1, line, destination);
  }

  /** Method to validate train number. This makes sure that the application runs smoothly and
   * contribute to give the user a warning of unreasonable input.
   *
   * @param trainNo the requested number of the specific train departure
   * @throws IllegalArgumentException if the requested train number isn't positive
   */
  private void validateTrainNo(int trainNo) throws IllegalArgumentException {
    if (trainNo < 1) {
      throw new IllegalArgumentException("Invalid input: The requested train number was below 1.");
    }
  }

  /** Method to validate the track of the train departure. This makes sure that the application
   * runs smoothly and contributes to give the user a warning of unreasonable input, which is
   * non-positive numbers. The user will be able to assign -1 to the track, but the system will
   * interpret this value to indicate that the track hasn't been assigned yet.
   *
   * @param track the requested track-value to validate
   * @throws IllegalArgumentException if the requested track is 0 or less than -1
   */
  private void validateTrack(int track) throws IllegalArgumentException {
    if (track < -1 || track == 0) {
      throw new IllegalArgumentException("Invalid input: the track number has to be positive.");
    }
  }

  /** Method to validate a string-input. This method checks whether the text-input is empty, and
   * throws if true.
   *
   * @param text the text to validate
   * @throws IllegalArgumentException if the text is empty
   */
  private void validateString(String text) throws IllegalArgumentException {
    if (text.isEmpty()) {
      throw new IllegalArgumentException("Invalid input: What was entered was empty and therefore "
          + "not accepted.");
    }
  }

  /** Method to validate delay. This method controls whether the delay would make the departure time
   * with the delay time make the train leave past midnight. It throws if true.
   *
   * @param delay the requested delay to validate
   * @throws IllegalArgumentException if the departure time with the delay time is past midnight
   */
  private void validateDelay(LocalTime delay) throws IllegalArgumentException {
    int totalHours = delay.getHour() + departureTime.getHour();
    int totalMinutes = delay.getMinute() + departureTime.getMinute() + totalHours * 60;
    int beforeMidnight = 23 * 60 + 59;

    if (totalMinutes > beforeMidnight) {
      throw new IllegalArgumentException("Invalid input: The time you set would make the train "
          + "depart past midnight, which this app is not made for.");
    }
  }

  /** Get-method for train number.
   *
   * @return the train number for the train departure
   */
  public int getTrainNo() {
    return trainNo;
  }

  /** Get-method for departure time.
   *
   * @return the departure time of the train departure, without delay
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /** Get-method for delay.
   *
   * @return the delay of the train departure
   */
  public LocalTime getDelay() {
    return delay;
  }

  /** Get-method for track.
   *
   * @return the track of the train departure
   */
  public int getTrack() {
    return track;
  }

  /** Get-method for line.
   *
   * @return the line of the train departure
   */
  public String getLine() {
    return line;
  }

  /** Get-method for destination.
   *
   * @return the destination of the train departure
   */
  public String getDestination() {
    return destination;
  }

  /** Get-method for departure time with delay. The method adds the departure time and delay
   * together through the timeWithDelay-variable inside the method. This makes it so that the
   * departure time-field will not be changed.
   *
   * @return a new value that is the departure time with delay
   */
  public LocalTime getDepartureTimeWithDelay() {
    LocalTime timeWithDelay =
        LocalTime.of(departureTime.getHour(), departureTime.getMinute());
    return timeWithDelay.plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

  /** Set-method for delay. Before setting the delay, the validateDelay-method is used to make sure
   * the train won't leave over midnight.
   *
   *
   * @param delay the requested delay for the train departure
   * @throws IllegalArgumentException if the departure time with the delay time is over midnight
   * @see #validateDelay(LocalTime)
   */
  public void setDelay(LocalTime delay) throws IllegalArgumentException {
    validateDelay(delay);
    this.delay = delay;
  }

  /** Set-method for track. Before setting the track of the departure, the method controls if the
   * track number is valid by using the validateTrack-method.
   *
   * @param track the requested track for the train departure.
   * @throws IllegalArgumentException if the track param is invalid, i.e. not positive except for -1
   * @see #validateTrack(int)
   */
  public void setTrack(int track) throws IllegalArgumentException {
    validateTrack(track);
    this.track = track;
  }

  @Override
  public String toString() {
    String strDelay;
    String strTrack;

    if (delay.equals(LocalTime.MIN)) {
      strDelay = "";
    } else {
      strDelay = String.valueOf(delay);
    }
    if (this.track == -1) {
      strTrack = "";
    } else {
      strTrack = String.valueOf(track);
    }

    return String.format("%-13s%-10s%-10s%-10s%-17s%-8s",
        departureTime, strDelay, line, trainNo, destination, strTrack);
  }
}
