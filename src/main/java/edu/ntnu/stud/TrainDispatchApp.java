package edu.ntnu.stud;

/** App class that will start the application.
 *
 * @author Therese Synnøve Rondeel
 */
public class TrainDispatchApp {
  /** Method that runs the application. It first initializes an instance of the UserInterface-class,
   * and then calls the init- and start-method of that class.
   *
   * @param args array of Strings that stores the Java command-line arguments
   * @see UserInterface
   */
  public static void main(String[] args) {
    UserInterface userInterface = new UserInterface();
    userInterface.init();
    userInterface.start();
  }
}
