package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TrainDepartureRegisterTest {
  private TrainDepartureRegister trainDepartureRegister;

  private final int TRAIN_NO1 = 123;
  private final int TRAIN_NO2 = 83;
  private final int TRAIN_NO3 = 172;
  private final int TRAIN_NO4 = 765;
  private final LocalTime DEPARTURE_TIME1 = LocalTime.of(8, 0);
  private final LocalTime DEPARTURE_TIME2 = LocalTime.of(6, 45);
  private final LocalTime DEPARTURE_TIME3 = LocalTime.of(17, 8);
  private final LocalTime DEPARTURE_TIME4 = LocalTime.of(13, 4);
  private final LocalTime NO_DELAY = LocalTime.MIN;
  private final LocalTime DELAY1 = LocalTime.of(1, 10);
  private final LocalTime DELAY2 = LocalTime.of(0, 5);
  private final int NO_TRACK = -1;
  private final int TRACK1 = 2;
  private final int TRACK3 = 4;
  private final int TRACK4 = 1;
  private final String LINE1 = "F6";
  private final String LINE2 = "R11";
  private final String LINE3 = "L2";
  private final String LINE4 = "F4";
  private final String DESTINATION1 = "Trondheim";
  private final String DESTINATION2 = "Skien";
  private final String DESTINATION3 = "Ski";
  private final String DESTINATION4 = "Bergen";

  @BeforeEach
  void setUp() {
    trainDepartureRegister = new TrainDepartureRegister();
    trainDepartureRegister.addDeparture(TRAIN_NO1, DEPARTURE_TIME1, TRACK1, LINE1, DESTINATION1);
    trainDepartureRegister.addDeparture(TRAIN_NO2, DEPARTURE_TIME2, LINE2, DESTINATION2);
    trainDepartureRegister.addDeparture(TRAIN_NO3, DEPARTURE_TIME3, TRACK3, LINE3, DESTINATION3);

    trainDepartureRegister.setCurrentTime(LocalTime.of(5, 30));
    
    trainDepartureRegister.setDelay(TRAIN_NO1, DELAY1);
    trainDepartureRegister.setDelay(TRAIN_NO2, DELAY2);
  }

  // This method was made by ChatGPT. Firstly, a new register is made. After this, the assertEquals
  // and assertNotNull is used to make sure that the register and current time are not of the null-
  // value, and that the current time is set to 00:00, which is LocalTime.MIN.
  @Test
  @DisplayName("Test constructor initialization works")
  void testConstructorInitialization() {
    TrainDepartureRegister register = new TrainDepartureRegister();
    assertNotNull(register.getTrainDepartureRegister());
    assertNotNull(register.getCurrentTime());
    assertEquals(LocalTime.MIN, register.getCurrentTime());
  }

  @Test
  @DisplayName("Test addDeparture() with track parameter works")
  void testAddDepartureWithTrackWithValidInput() {
    trainDepartureRegister.addDeparture(TRAIN_NO4, DEPARTURE_TIME4, TRACK4, LINE4, DESTINATION4);
    
    TrainDeparture addedDeparture =
        trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO4);

    assertNotNull(addedDeparture);
    assertEquals(TRAIN_NO4, addedDeparture.getTrainNo());
    assertEquals(DEPARTURE_TIME4, addedDeparture.getDepartureTime());
    assertEquals(NO_DELAY, addedDeparture.getDelay());
    assertEquals(TRACK4, addedDeparture.getTrack());
    assertEquals(LINE4, addedDeparture.getLine());
    assertEquals(DESTINATION4, addedDeparture.getDestination());
  }

  @Test
  @DisplayName("Test addDeparture() without track parameter works")
  void testAddDepartureWithoutTrackWithValidInput() {
    trainDepartureRegister.addDeparture(TRAIN_NO4, DEPARTURE_TIME4, LINE4, DESTINATION4);

    TrainDeparture addedDeparture =
        trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO4);

    assertNotNull(addedDeparture);
    assertEquals(TRAIN_NO4, addedDeparture.getTrainNo());
    assertEquals(DEPARTURE_TIME4, addedDeparture.getDepartureTime());
    assertEquals(NO_DELAY, addedDeparture.getDelay());
    assertEquals(NO_TRACK, addedDeparture.getTrack());
    assertEquals(LINE4, addedDeparture.getLine());
    assertEquals(DESTINATION4, addedDeparture.getDestination());
  }

  @Test
  @DisplayName("Test addDeparture() with track parameter throws if the requested train number is "
      + "already in the register")
  void testAddDepartureWithTrackWithInvalidTrainNo() {
    int invalidTrainNo = TRAIN_NO1;

    assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister
        .addDeparture(invalidTrainNo, DEPARTURE_TIME4, TRACK4, LINE4, DESTINATION4));
  }

  @Test
  @DisplayName("Test addDeparture() with track parameter throws if the requested departure time is "
      + "before the current time")
  void testAddDepartureWithTrackWithInvalidDepartureTime() {
    LocalTime invalidDepartureTime = LocalTime.of(4, 50);

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.addDeparture(TRAIN_NO4, invalidDepartureTime, TRACK4, LINE4, DESTINATION4));
  }

  @Test
  @DisplayName("Test addDeparture() without track parameter throws if the requested train number is"
      + " already in the register")
  void testAddDepartureWithoutTrackWithInvalidTrainNo() {
    int invalidTrainNo = TRAIN_NO1;

    assertThrows(IllegalArgumentException.class, () -> trainDepartureRegister
        .addDeparture(invalidTrainNo, DEPARTURE_TIME4, LINE4, DESTINATION4));
  }

  @Test
  @DisplayName("Test addDeparture() without track parameter throws if the requested departure time "
      + "is before the current time")
  void testAddDepartureWithoutTrackWithInvalidDepartureTime() {
    LocalTime invalidDepartureTime = LocalTime.of(4, 50);

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.addDeparture(TRAIN_NO4, invalidDepartureTime, LINE4, DESTINATION4));
  }

  @Test
  @DisplayName("Test getDepartureByTrainNo() with valid train number")
  void testGetDepartureByTrainNoWithValidTrainNo() {
    TrainDeparture correctDeparture = trainDepartureRegister.getTrainDepartureRegister()
        .get(TRAIN_NO1);
    TrainDeparture departureThroughMethod = trainDepartureRegister.getDepartureByTrainNo(TRAIN_NO1);

    assertNotNull(departureThroughMethod);
    assertEquals(correctDeparture, departureThroughMethod);
    assertEquals(correctDeparture.getTrainNo(), departureThroughMethod.getTrainNo());
    assertEquals(correctDeparture.getDepartureTime(), departureThroughMethod.getDepartureTime());
    assertEquals(correctDeparture.getTrack(), departureThroughMethod.getTrack());
    assertEquals(correctDeparture.getLine(), departureThroughMethod.getLine());
    assertEquals(correctDeparture.getDestination(), departureThroughMethod.getDestination());
  }

  @Test
  @DisplayName("Test getDepartureByTrainNo() with invalid train number returns null-value")
  void testGetDepartureByTrainNoWithInvalidTrainNo() {
    int invalidTrainNo = 987;

    assertNull(trainDepartureRegister.getDepartureByTrainNo(invalidTrainNo));
  }

  @Test
  @DisplayName("Test setCurrentTime() with valid input")
  void testSetCurrentTimeWithValidInput() {
    LocalTime validTime = LocalTime.of(7, 0);

    trainDepartureRegister.setCurrentTime(validTime);

    assertEquals(validTime, trainDepartureRegister.getCurrentTime());
  }

  @Test
  @DisplayName("Test setCurrentTime() with invalid time, i.e. time before the current one, throws")
  void testSetCurrentTimeWithInvalidInput() {
    LocalTime invalidTime = LocalTime.of(3, 30);

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.setCurrentTime(invalidTime));
  }

  @Test
  @DisplayName("Test setTrack() with valid input")
  void testSetTrackWithValidInput() {
    int validTrack = 1;

    trainDepartureRegister.setTrack(TRAIN_NO1, validTrack);
    TrainDeparture departure1 = trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO1);

    assertEquals(validTrack, departure1.getTrack());
  }

  @Test
  @DisplayName("Test setTrack() with invalid train number and valid track throws")
  void testSetTrackWithInvalidTrainNo() {
    int invalidTrainNo = 987;
    int track = 1;

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.setTrack(invalidTrainNo, track));
  }

  @Test
  @DisplayName("Test setTrack() with valid train number and invalid track throws")
  void testSetTrackWithInvalidTrack() {
    int invalidTrack = -2;

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.setTrack(TRAIN_NO3, invalidTrack));
  }

  @Test
  @DisplayName("Test setDelay() on train 3 with valid input")
  void testSetDelayWithValidInput() {
    LocalTime delayForTrain3 = LocalTime.of(1, 5);

    trainDepartureRegister.setDelay(TRAIN_NO3, delayForTrain3);
    TrainDeparture delayedDeparture =
        trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO3);

    assertEquals(delayForTrain3, delayedDeparture.getDelay());
  }

  @Test
  @DisplayName("Test setDelay() with invalid train number and valid delay throws")
  void testSetDelayWithInvalidTrainNo() {
    int invalidTrainNo = 987;
    LocalTime delay = LocalTime.of(0, 30);

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.setDelay(invalidTrainNo, delay));
  }

  @Test
  @DisplayName("Test setDelay() on train 3 with valid train number and invalid delay throws")
  void testSetDelayWithInvalidDelay() {
    LocalTime invalidDelayForTrain3 = LocalTime.of(23, 0);

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.setDelay(TRAIN_NO3, invalidDelayForTrain3));
  }

  @Test
  @DisplayName("Test removeDepartures() works")
  void testRemoveDepartures() {
    LocalTime newCurrentTime = LocalTime.of(10, 30);
    long amountDepartureTimeBeforeNewCurrentTime =
        trainDepartureRegister.getTrainDepartureRegister().values().stream()
        .filter(trainDeparture
            -> trainDeparture.getDepartureTimeWithDelay().isBefore(newCurrentTime))
        .count();
    int departuresInRegisterBeforeNewTime =
        trainDepartureRegister.getTrainDepartureRegister().size();

    trainDepartureRegister.setCurrentTime(newCurrentTime);
    int departuresInRegisterAfterNewTime =
        trainDepartureRegister.getTrainDepartureRegister().size();

    int amountDeparturesRemoved =
        departuresInRegisterBeforeNewTime - departuresInRegisterAfterNewTime;
    assertEquals(amountDepartureTimeBeforeNewCurrentTime,
        amountDeparturesRemoved);
  }

  @Test
  @DisplayName("Test removeDeparturesByTrainNo() with valid input")
  void testRemoveDeparturesByTrainNoWithValidInput() {
    trainDepartureRegister.removeDepartureByTrainNo(TRAIN_NO1);

    assertFalse(trainDepartureRegister.getTrainDepartureRegister().containsKey(TRAIN_NO1));
  }

  @Test
  @DisplayName("Test removeDeparturesByTrainNo() with invalid train number throws")
  void testRemoveDeparturesByTrainNoWithInvalidInput() {
    int invalidTrainNo = 987;

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.removeDepartureByTrainNo(invalidTrainNo));
  }

  @Test
  @DisplayName("Test checkTrainDepartureExists() returns true with valid train number")
  void testCheckTrainDepartureExistsWithValidTrainNo() {
    assertTrue(trainDepartureRegister.checkTrainDepartureExists(TRAIN_NO1));
  }

  @Test
  @DisplayName("Test checkTrainDepartureExists() returns false with invalid train number")
  void testCheckTrainDepartureExistsWithInvalidTrainNo() {
    int invalidTrainNo = 987;

    assertFalse(trainDepartureRegister.checkTrainDepartureExists(invalidTrainNo));
  }

  @Test
  @DisplayName("Test getTrainDeparturesByDestination() with valid destination")
  void testGetTrainDeparturesByDestinationWithValidDestination() {
    String destination = DESTINATION1;

    trainDepartureRegister.addDeparture(TRAIN_NO4, DEPARTURE_TIME4, TRACK4, LINE4, destination);

    long amountDeparturesToDestination = trainDepartureRegister.getTrainDepartureRegister()
        .values().stream()
        .filter(trainDeparture -> trainDeparture.getDestination().equals(destination))
        .count();

    assertEquals(amountDeparturesToDestination,
        trainDepartureRegister.getDeparturesByDestination(destination).size());
  }

  @Test
  @DisplayName("Test getDeparturesByDestination() with invalid destination returns null-value")
  void testGetDeparturesByDestinationWithInvalidDestination() {
    String invalidDestination = "Stockholm";

    assertNull(trainDepartureRegister.getDeparturesByDestination(invalidDestination));
  }

  @Test
  @DisplayName("Test getDeparturesByTrack() with valid track")
  void testGetDeparturesByTrackWithValidTrack() {
    int track = TRACK1;

    trainDepartureRegister.addDeparture(TRAIN_NO4, DEPARTURE_TIME4, track, LINE4, DESTINATION4);

    long amountDeparturesFromTrack = trainDepartureRegister.getTrainDepartureRegister()
        .values().stream()
        .filter(trainDeparture -> trainDeparture.getTrack() == track)
        .count();

    assertEquals(amountDeparturesFromTrack, trainDepartureRegister.getDeparturesByTrack(track).size());
  }

  @Test
  @DisplayName("Test getDeparturesByTrack() with track -1 returns all unassigned departures")
  void testGetDeparturesByTrackWithTrackNegative1() {
    trainDepartureRegister.addDeparture(TRAIN_NO4, DEPARTURE_TIME4, NO_TRACK, LINE4, DESTINATION4);

    long amountDeparturesFromTrack = trainDepartureRegister.getTrainDepartureRegister()
        .values().stream()
        .filter(trainDeparture -> trainDeparture.getTrack() == NO_TRACK)
        .count();

    assertEquals(amountDeparturesFromTrack,
        trainDepartureRegister.getDeparturesByTrack(NO_TRACK).size());
  }

  @Test
  @DisplayName("Test getDeparturesByTrack() with invalid track returns null-value")
  void testGetDeparturesByTrackWithInvalidTrack() {
    int invalidTrack = -2;

    assertNull(trainDepartureRegister.getDeparturesByTrack(invalidTrack));
  }

  @Test
  @DisplayName("Test getDeparturesWithTrack() returns right amount of departures")
  void testGetDeparturesWithTrack() {
    int registerSize = trainDepartureRegister.getTrainDepartureRegister().size();
    int departuresWithTrackSize = trainDepartureRegister.getDeparturesWithTrack().size();
    int expectedDifference = 1;

    assertEquals(expectedDifference, registerSize - departuresWithTrackSize);
  }

  @Test
  @DisplayName("Test sortByTime() sorts correctly")
  void testSortByTimeSortsCorrectly() {
    TrainDeparture earliestDeparture = trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO2);
    TrainDeparture middleDeparture = trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO1);
    TrainDeparture latestDeparture = trainDepartureRegister.getTrainDepartureRegister().get(TRAIN_NO3);

    List<TrainDeparture> registerAsList =
        new ArrayList<>(trainDepartureRegister.getTrainDepartureRegister().values());
    trainDepartureRegister.sortByTime(registerAsList);
    assertEquals(registerAsList.get(0), earliestDeparture);
    assertEquals(registerAsList.get(1), middleDeparture);
    assertEquals(registerAsList.get(2), latestDeparture);
  }
}