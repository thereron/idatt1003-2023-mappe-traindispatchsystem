package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TrainDepartureTest {
  private TrainDeparture trainDeparture;

  private final int TRAIN_NR = 123;
  private final LocalTime DEPARTURE_TIME = LocalTime.of(8, 0);
  private final LocalTime NO_DELAY = LocalTime.MIN;
  private final LocalTime DELAY = LocalTime.of(1, 10);
  private final LocalTime DEPARTURE_TIME_WITH_DELAY = LocalTime.of(9, 10);
  private final int NO_TRACK = -1;
  private final int TRACK = 2;
  private final String LINE = "F6";
  private final String DESTINATION = "Oslo";

  @BeforeEach
  public void setUp(){
    trainDeparture = new TrainDeparture(TRAIN_NR, DEPARTURE_TIME, TRACK, LINE, DESTINATION);
  }

  // This method was made by the help of ChatGPT. It had the arrange-part of AAA also
  // included, but these were since made into constants. The act-part happens in the
  // beforeEach-method when creating an object of the train departure class. The assertEquals were
  // implemented by the help of ChatGPT. Other methods in the test-classes using assertEquals
  // have been inspired by this method. The assertEquals in this method makes sure that the desired
  // value for departure time, as an example, is equal to the attribute of the object.
  @Test
  @DisplayName("Test constructor with track parameter with valid input")
  public void testConstructorWithTrackParameterWithValidInput() {
    assertEquals(TRAIN_NR,  trainDeparture.getTrainNo());
    assertEquals(DEPARTURE_TIME, trainDeparture.getDepartureTime());
    assertEquals(NO_DELAY, trainDeparture.getDelay());
    assertEquals(TRACK, trainDeparture.getTrack());
    assertEquals(LINE, trainDeparture.getLine());
    assertEquals(DESTINATION, trainDeparture.getDestination());
  }

  @Test
  @DisplayName("Test constructor without track parameter with valid input")
  void testConstructorWithoutTrackParameterWithValidInput() {
    TrainDeparture trainDepartureWithoutTrack =
        new TrainDeparture(TRAIN_NR, DEPARTURE_TIME, LINE, DESTINATION);

    assertEquals(TRAIN_NR,  trainDepartureWithoutTrack.getTrainNo());
    assertEquals(DEPARTURE_TIME, trainDepartureWithoutTrack.getDepartureTime());
    assertEquals(NO_DELAY, trainDepartureWithoutTrack.getDelay());
    assertEquals(NO_TRACK,trainDepartureWithoutTrack.getTrack());
    assertEquals(LINE,trainDepartureWithoutTrack.getLine());
    assertEquals(DESTINATION, trainDepartureWithoutTrack.getDestination());
  }

  // This method was made by the help of ChatGPT. Firstly, the invalid train number is assigned.
  // The other parameter-inputs were assigned as constants. Both act and assert are executed
  // simultaneously by asserting that the constructor will throw if there is any invalid input when
  // creating a new object of the TrainDeparture-class.
  // All other methods in the test-classes using assertThrows were inspired by this method.
  @Test
  @DisplayName("Test validateTrainNo() with invalid train number throws")
  void testValidateTrainNoWithInvalidTrainNo() {
    int invalidTrainNo = 0;

    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(invalidTrainNo, DEPARTURE_TIME, TRACK, LINE, DESTINATION));
  }

  @Test
  @DisplayName("Test validateTrack() with invalid track throws")
  void testValidateTrackWithInvalidTrack() {
    int invalidTrack = -2;

    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(TRAIN_NR, DEPARTURE_TIME, invalidTrack, LINE, DESTINATION));
  }

  @Test
  @DisplayName("Test validateValues() with invalid line throws")
  void testValidateTextWithInvalidLine() {
    String invalidLine = "";

    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(TRAIN_NR, DEPARTURE_TIME, TRACK, invalidLine, DESTINATION));
  }

  @Test
  @DisplayName("Test validateValues() with invalid destination throws")
  void testValidateTextWithInvalidDestination() {
    String invalidDestination = "";

    assertThrows(IllegalArgumentException.class,
        () -> new TrainDeparture(TRAIN_NR, DEPARTURE_TIME, TRACK, LINE, invalidDestination));
  }

  @Test
  @DisplayName("Test getDepartureTimeWithDelay() returns correct value")
  void testGetDepartureTimeWithDelay() {
    trainDeparture.setDelay(DELAY);

    assertEquals(DEPARTURE_TIME_WITH_DELAY, trainDeparture.getDepartureTimeWithDelay());
  }

  @Test
  @DisplayName("Test setDelay() with valid input")
  void testSetDelayWithValidInput() {
    trainDeparture.setDelay(DELAY);

    assertEquals(DELAY, trainDeparture.getDelay());
  }

  @Test
  @DisplayName("Test setDelay() doesn't let the requested delay make departure time with delay "
      + "go over midnight")
  void testSetDelayDoesntGoOverMidnight() {
    LocalTime invalidDelay = LocalTime.of(16, 0);

    assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay(invalidDelay));
  }

  @Test
  @DisplayName("Test setTrack() with valid input")
  void testSetTrackWithValidInput() {
    int validTrack = 4;

    trainDeparture.setTrack(validTrack);

    assertEquals(validTrack, trainDeparture.getTrack());
  }

  @Test
  @DisplayName("Test setTrack() with invalid input throws")
  void testSetTrackWithInvalidInput() {
    int invalidTrack = -2;

    assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(invalidTrack));
  }
}