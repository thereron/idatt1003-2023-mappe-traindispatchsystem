# Portfolio project IDATT1003 - 2023
STUDENT NAME = Therese Synnøve Rondeel  
STUDENT ID = 111779

## Project description
This product is a result of an assessment in subject IDATT1003 at NTNU. It is a simplified version of a train dispatch
system.

## Project structure
The application may be found in src/main/java/edu/ntnu/stud. JUnit-test-classes may be found in src/main/java/edu/ntnu/stud.

## Link to repository
[Link](https://gitlab.stud.idi.ntnu.no/thereron/idatt1003-2023-mappe-traindispatchsystem)

## How to run the project
For running the application, locate the TrainDispatchApp in src/main/java/edu/ntnu/stud, and run. A console with a 
menu-based interface will be shown, and should be intuitive to use. After entering the prompted input, the application 
should print the requested information, or do the requested option.


## How to run the tests
Locate the test-classes in src/main/java/edu/ntnu/stud and run these. You may run all of the tests at once, or run them
one by one.

## References
[ChatGPT](https://chat.openai.com/)

